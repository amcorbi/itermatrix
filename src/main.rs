
extern crate rayon;
use rayon::prelude::*;

// Random numbers //-------------------------------------------------------
extern crate rand;
use rand::prelude::*;
//-------------------------------------------------------------------------

#[derive(Debug, Clone)]
struct Matrix {
    cr: usize,
    cc: usize,
    data: Vec<Vec<u32>>
}

struct MatrixIter<'a> {
    cr: usize,
    cc: usize,
    the_matrix: &'a Matrix
}

impl<'a> Iterator for MatrixIter<'a> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        let max = self.the_matrix.data.len();

        if self.cr < max && self.cc < max {
            let result = self.the_matrix.data[self.cr][self.cc];
            self.cc += 1;
            if self.cc == max {
                self.cc = 0;
                self.cr += 1;
            }
            return Some(result);
        }
        None
    }
}

impl Matrix {
    fn new(s: u32) -> Matrix {

        let mut data: Vec<Vec<u32>> = Vec::new();
        for _r in 0..s {
            let mut r = Vec::<u32>::new();
            for _c in 0..s {
                //r.push(_r + _c);
                r.push(random());
            }
            data.push(r);
        }
        
        let m = Matrix {
            cr: 0,
            cc: 0,
            data
        };

        m
    }

    fn iter(&self) -> MatrixIter {
        let mi = MatrixIter {
            cr: 0, cc: 0,
            the_matrix: &self
        };

        mi
    }
}


fn main() {
    let m = Matrix::new(3000);
    println!("Hello, from itermatrix! {:?}", m);
    println!("Let's iterate it...");
    for e in m.par_iter() {
        println!("E = {}", e);
    }
    println!("Hello, from itermatrix! {:?}", m);
}
